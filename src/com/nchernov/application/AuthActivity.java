package com.nchernov.application;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutionException;

import com.nchernov.communicator.Communicator;
import com.nchernov.communicator.NetworkStatusUtils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AuthActivity extends Activity {

	private static final int ACTIVITY_TABS = 0;
	protected static final String SERVICE_HOST = "http://ihearditfirst.rosberry.com/api.php";
	EditText inputLogin = null;
	EditText inputPassword = null;
	Button buLogin = null;
	private Communicator communicator = null;
	LoginTask loginTask = null;
	
	private String DEBUG_LOGIN = "user";
	private String DEBUG_PASSWORD = "password";
	
	private String toastErrorMsg = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu);
		initGUI();
		defineButtonLogic();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}
	
	private void initGUI() {
		buLogin = (Button) findViewById(R.id.buLogin);
		inputLogin = (EditText) findViewById(R.id.inputLogin);
		inputPassword = (EditText) findViewById(R.id.inputPassword);
	}
	
	private void callActivity(int screenId, String tokenString) {
		switch(screenId) {
		case ACTIVITY_TABS:
			Intent tabsIntent = new Intent();
			tabsIntent.setClass(getApplicationContext(), MainScreenAcivity.class);
			tabsIntent.putExtra(Namespace.KEY_TOKEN, tokenString);
			startActivity(tabsIntent);
			default:
				break;
		}
	}
	
	private void defineButtonLogic() {
		
		buLogin.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String mLogin = null;
				String mPassword = null;
				mLogin = inputLogin.getText() + "";
				mPassword = inputPassword.getText() + "";
				if (!NetworkStatusUtils.isConnectingToInternet(getApplicationContext())) {
					Toast.makeText(getApplicationContext(), "Cannot get connection. Please, check internet connection and try again.", Toast.LENGTH_LONG).show();
					return;
				}
				// Send post-request
				// receive answer
					try {
						loginTask = new LoginTask(AuthActivity.this);
						String token = loginTask.execute(SERVICE_HOST, mLogin, mPassword).get();
						
						
						if (token == null || token.isEmpty()) {
							Toast.makeText(getApplicationContext(), "Cannot authorize to service..", Toast.LENGTH_LONG).show();
						} else {
							Toast.makeText(getApplicationContext(), "Success! Authorized: ok.", Toast.LENGTH_LONG).show();
							callActivity(ACTIVITY_TABS, token);
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ExecutionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
		});
	}
	
	protected void showToast(String msg) {
		toastErrorMsg = msg;
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(getApplicationContext(), toastErrorMsg, Toast.LENGTH_LONG * 2).show();
			}
		});
	}
	

}
