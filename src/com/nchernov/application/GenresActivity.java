package com.nchernov.application;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.nchernov.communicator.Communicator;
import com.nchernov.communicator.NetworkStatusUtils;
import com.nchernov.models.Genre;
import com.nchernov.models.GenreManager;
import com.nchernov.models.GenresDAO;
import com.nchernov.models.GenresDAOImpl;
import com.nchernov.views.GenreItemAdapter;

public class GenresActivity extends Activity {

	private List<Genre> mGenres = null;
	private List<Genre> mGenresFound = null;
	GenreManager mGenreManager = null;
	
	private ArrayAdapter<Genre> mGenresAdapter = null;
	private Communicator mCommunicator = null;
	
	private ListView mGenresListView = null;
	
	private RequestGenresTask mGenresTask = null;
	
	private Context mParentActivityContext = null;
	
	private Spinner mSpinner = null;
	private ProgressDialog spinner;
	
	GenresDAO mGenresDAO = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.genres_screen);
		Bundle b = this.getIntent().getExtras();
		String token = b.getString(Namespace.KEY_TOKEN);
		mGenresListView = (ListView) findViewById(R.id.lvGenres);
		mGenresDAO = new GenresDAOImpl(getApplicationContext());
		
		loadLocalGenres();
		
		if (!NetworkStatusUtils.isConnectingToInternet(getApplicationContext())) {
			Toast.makeText(getApplicationContext(), "Cannot get connection. Please, check internet connection and try again.", Toast.LENGTH_LONG).show();
		} else {
			Toast.makeText(getApplicationContext(), "Connecting to service.. Updating genres list..", Toast.LENGTH_LONG);
			loadGenres(token);
		}
		
		refreshList(mGenres);
		
		mGenreManager = new GenreManager(mGenres);
		
		
		// Setting change text logic
		EditText searchInput = (EditText) findViewById(R.id.searchGenre);
		searchInput.addTextChangedListener(new TextWatcher(){
	        public void afterTextChanged(Editable s) {
	        	if (mGenres == null || mGenres.size() == 0) return;
	            String inputText = s.toString();
	            if (inputText == null || inputText.isEmpty()) {
	            	mGenresFound = new ArrayList<Genre>();
	            	mGenresFound.addAll(mGenres);
	            } else {
	            	mGenresFound = mGenreManager.findByStartSubstring(inputText);
	            }
	           
	            refreshList(mGenresFound);
	        }
	        public void beforeTextChanged(CharSequence s, int start, int count, int after){}
	        public void onTextChanged(CharSequence s, int start, int before, int count){}
	    });
	}
	
	private void loadGenres(String token) {
		mCommunicator = new Communicator(Namespace.SERVICE_HOST, null);
		try {
			spinner = new ProgressDialog(this);
            spinner.setMessage("�������� ������ ������..");
			mGenresTask = new RequestGenresTask(this, spinner);
			String url = Namespace.SERVICE_HOST;
			mGenres = mGenresTask.execute(url, token).get();
			if (mGenres == null) {
				Toast.makeText(getApplicationContext(), "Cannot get genres list from service. Please, try later or re-login", Toast.LENGTH_LONG).show();
				
			} else {
				Toast.makeText(getApplicationContext(), "Success! Saving data to local storage..", Toast.LENGTH_LONG).show();
				saveToLocal(mGenres);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		spinner.cancel();
	}
	
	/**
	 * Get local data from db - offline mode
	 */
	private void loadLocalGenres() {
		mGenres = (List<Genre>) mGenresDAO.loadGenres();
		refreshList(mGenres);
	}
	
	/**
	 * Simply replace table data by new one
	 */
	private void saveToLocal(List<Genre> genres) {
		mGenresDAO.saveGenres(genres);
	}
	
	private void refreshList(List<Genre> subSet) {
		mGenresAdapter = new GenreItemAdapter(getApplicationContext(), R.layout.genre_row, subSet);
		mGenresListView.setAdapter(mGenresAdapter);
		mGenresAdapter.notifyDataSetChanged();
	}
}
