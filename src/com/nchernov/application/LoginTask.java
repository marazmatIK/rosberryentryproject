package com.nchernov.application;

import java.io.UnsupportedEncodingException;

import org.apache.http.conn.ConnectTimeoutException;

import com.google.android.gms.maps.MapView;
import com.nchernov.communicator.AuthException;
import com.nchernov.communicator.Communicator;
import com.nchernov.communicator.WrongRequestException;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

class LoginTask extends AsyncTask<String, String, String> {
    private ProgressDialog spinner;
	private Context mContext;
	private Communicator communicator = null;

    public LoginTask(Context context) {
    	mContext = context;
    	
    }
    @Override
    protected void onPreExecute() {
            // ������� �� ������� ������������ ProgressDialog 
            // ����� �� ������� ��� �������� ��������
            // ���� ����� ����������� � UI ������
            spinner = new ProgressDialog(mContext);
            spinner.setMessage("���� ��������...");
            spinner.show();
    }

	@Override
	protected String doInBackground(String... params) {
		String url = params[0];
		communicator  = new Communicator(url, null);
		String token = null;
		try {
			String mLogin  = params[1];
			String  mPassword = params[2];
			token = communicator.login(mPassword, mLogin);
		} catch (UnsupportedEncodingException e) {
			spinner.dismiss();
			//Toast.makeText(mContext,  Toast.LENGTH_LONG).show();
			((AuthActivity)mContext).showToast("Sorry. Cannot auth to server: " + url + ". " + e.getMessage() + ". Please, try again.");
		} catch (ConnectTimeoutException e) {
			spinner.dismiss();
			((AuthActivity)mContext).showToast("Cannot connect to server. Please, check Internet connection and try again.");
			//Toast.makeText(mContext, , Toast.LENGTH_LONG).show();
		} catch (WrongRequestException e) {
			spinner.dismiss();
			((AuthActivity)mContext).showToast("Cannot connect to server. Please, contact with technical support team and describe Your problem in steps.");
			//Toast.makeText(mContext, "Cannot connect to server. Please, contact with technical support team and describe Your problem in steps.", Toast.LENGTH_LONG).show();
		} catch (AuthException e) {
			spinner.dismiss();
			((AuthActivity)mContext).showToast("Cannot connect to server. Wrong credentials. Please, check login and password and try again");
			//Toast.makeText(mContext, "Cannot connect to server. Wrong credentials. Please, check login and password and try again", Toast.LENGTH_LONG).show();
		}
		Log.d("AsyncTask: ", token == null ? "null" : token);
		return token;
	}
	
	@Override
    protected void onPostExecute(String result) {
            Log.d("AsyncTask: ", result == null ? "null" : result);
    	spinner.dismiss();
    }


}
