package com.nchernov.application;

import android.app.Activity;
import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;

public class MainScreenAcivity extends TabActivity {

	private static final String TAB_1 = "tab1";
	private static final String TAB_2 = "tab2";
	private static final String TAB_3 = "tab3";
	
	private TabHost mTabHost = null;
	private TabSpec firstTab = null;
	private TabSpec secondTab = null;
	private TabSpec thirdTab = null;
	
	private String mToken = null;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tabs_screen);
		Bundle b = this.getIntent().getExtras();
		mToken = b.getString(Namespace.KEY_TOKEN);
        
        initTabs(getResources().getString(R.string.tabOne), getResources().getString(R.string.tabTwo), getResources().getString(R.string.tabThree));
        setTabLogic();
	}
	
	private void setTabLogic() {
		mTabHost.setOnTabChangedListener(new OnTabChangeListener() {
			
			@Override
			public void onTabChanged(String tabId) {
				if (tabId.equals(TAB_1)) {
					
				} else if (tabId.equals(TAB_2)) {
					
				} else if (tabId.equals(TAB_3)) {
					return;
				}
			}
		});
		
	}

	private void initTabs(String tabOne, String tabTwo, String tabThree) {
		mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        firstTab = mTabHost.newTabSpec(TAB_1);
        firstTab.setIndicator(tabOne); // getResources().getString(R.string.tabOne)
        firstTab.setContent(android.R.id.tabcontent);
        secondTab = mTabHost.newTabSpec(TAB_2);
        secondTab.setIndicator(tabTwo);
        secondTab.setContent(android.R.id.tabcontent);
        
        thirdTab = mTabHost.newTabSpec(TAB_3);
        thirdTab.setIndicator(tabThree);
        thirdTab.setContent(android.R.id.tabcontent);
        mTabHost.addTab(firstTab);
        mTabHost.addTab(secondTab);
        mTabHost.addTab(thirdTab);
        
        firstTab.setContent(new Intent(this, StaticTextActivity.class));
        secondTab.setContent(new Intent(this, GenresActivity.class).putExtra(Namespace.KEY_TOKEN, mToken));
        thirdTab.setContent(new Intent(this, MapActivity.class));
        mTabHost.setCurrentTab(2);
        mTabHost.setCurrentTab(0);
	}
	
	
}
