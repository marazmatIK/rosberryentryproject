package com.nchernov.application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import com.nchernov.views.CustomLocationListener;

@SuppressLint("SetJavaScriptEnabled")
public class MapActivity extends Activity {
	String mime = "text/html";
	String encoding = "utf-8";
	
	private static final String PLACEHOLDER_LONGITUDE = "[LONG]";
	private static final String PLACEHOLDER_LATTITUDE = "[LAT]";
	
	private static final String HTML_LOCATION_FOUND = "location_found.html";
	private static final String HTML_LOCATION_NOT_FOUND = "location_not_found.html";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);
		
		initMap();
	}
	
	private Location getSelfLocation() {
		LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        CustomLocationListener locationListener = new CustomLocationListener();  
		Location loc = null;
		LocationManager locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		
		Criteria locationCriteria = new Criteria();
		locationCriteria.setAccuracy(Criteria.ACCURACY_FINE);
		locationCriteria.setPowerRequirement(Criteria.POWER_LOW);
		locationCriteria.setAltitudeRequired(false);
		locationCriteria.setBearingRequired(false);
		locationCriteria.setSpeedRequired(false);
		locationCriteria.setCostAllowed(true);
		
		String locationProvider = locManager.getBestProvider(locationCriteria, false);
		LocationProvider bestLocationProvider = locManager.getProvider(locationProvider);
		Location lastKnownLocation = locManager.getLastKnownLocation(locationProvider);
		
		loc = lastKnownLocation;
		
		
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, locationListener);
		Location updatedLoc = locationListener.getLocation();
		loc = updatedLoc == null ? loc : updatedLoc;
		
		return loc;
	}
	
	@SuppressLint({ "SetJavaScriptEnabled", "SdCardPath" })
	private void initMap() {
		WebView javaScriptMapView = (WebView) findViewById(R.id.javascriptMap);
		WebSettings webSettings = javaScriptMapView.getSettings();
		webSettings.setJavaScriptEnabled(true);
		javaScriptMapView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		javaScriptMapView.getSettings().setBuiltInZoomControls(true);
		javaScriptMapView.getSettings().setJavaScriptEnabled(true);
		javaScriptMapView.getSettings().setGeolocationDatabasePath("/data/data/databases/");
		
        Toast.makeText(getApplicationContext(), "Defining location..", Toast.LENGTH_LONG).show();
		Location loc = getSelfLocation();
		String html = null;
		if (loc == null) {
			html = prepareHTML(false);
			Toast.makeText(getApplicationContext(), "Sorry, cannot find location..", Toast.LENGTH_LONG).show();
			html = html.replace(PLACEHOLDER_LATTITUDE, 0.0 + "").replace(PLACEHOLDER_LONGITUDE, 0.0 + "");
			//javaScriptMapView.loadUrl(HTML_LOCATION_NOT_FOUND);
		} else {
			html = prepareHTML(true);
			html = html.replace(PLACEHOLDER_LATTITUDE, loc.getLatitude() + "").replace(PLACEHOLDER_LONGITUDE, loc.getLongitude() + "");
			//javaScriptMapView.loadUrl(HTML_LOCATION_FOUND);
		} 
		javaScriptMapView.loadDataWithBaseURL(null, html, mime, encoding, null);
		
	}
	
	private String prepareHTML(boolean locationFound) {
		String html = getResources().getString(R.string.javascript_map);
		String fileName = locationFound ? HTML_LOCATION_FOUND : HTML_LOCATION_NOT_FOUND;
		StringBuilder htmlBuilder = new StringBuilder();
		try {
		    BufferedReader reader = new BufferedReader(
		        new InputStreamReader(getAssets().open(fileName), "UTF-8")); 

		    // do reading, usually loop until end of file reading 
		    String mLine = reader.readLine();
		    while (mLine != null) {
		    	htmlBuilder.append(mLine);
		       mLine = reader.readLine(); 
		    }

		    reader.close();
		} catch (IOException e) {
			
		}
		
		html = htmlBuilder.toString();
		htmlBuilder.setLength(0);
		
		return html;
		
	}
}
