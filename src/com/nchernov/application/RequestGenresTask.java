package com.nchernov.application;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.conn.ConnectTimeoutException;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.nchernov.communicator.AuthException;
import com.nchernov.communicator.Communicator;
import com.nchernov.communicator.WrongRequestException;
import com.nchernov.models.Genre;
import com.nchernov.views.GenreItemAdapter;

public class RequestGenresTask extends AsyncTask<String, String, List<Genre>> {

	private ProgressDialog spinner;
	private Context mContext;
	private List<Genre> mGenres;
	private Communicator mCommunicator;

    public RequestGenresTask(Context context, ProgressDialog dialog) {
    	mContext = context;
    	spinner = dialog;
    }
    
    @Override
    protected void onPreExecute() {
            spinner.show();
    }
	
	@Override
	protected List<Genre> doInBackground(String... params) {
		String url = params[0];
		String token = params[1];
		
		mCommunicator = new Communicator(url, null);
		try {
			mGenres = mCommunicator.requestForGenres(token);
			
		} catch (UnsupportedEncodingException e) {
			Toast.makeText(mContext, "Sorry. Cannot load genres: " + e.getMessage(), Toast.LENGTH_LONG).show();
		} catch (ConnectTimeoutException e) {
			Toast.makeText(mContext, "Cannot connect to server. Please, check Internet connection and try again.", Toast.LENGTH_LONG).show();
		} catch (WrongRequestException e) {
			Toast.makeText(mContext, "Cannot connect to server. Possible bad token.", Toast.LENGTH_LONG).show();
		} catch (AuthException e) {
			Toast.makeText(mContext, "Cannot connect to server. Please, contact with technical support team and describe Your problem in steps.", Toast.LENGTH_LONG).show();
		}
		return mGenres;
	}
	
	@Override
    protected void onPostExecute(List<Genre> result) {
    	spinner.dismiss();
    }

}
