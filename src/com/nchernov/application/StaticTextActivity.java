package com.nchernov.application;

import android.app.Activity;
import android.os.Bundle;

public class StaticTextActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.static_tab);
	}
}
