package com.nchernov.communicator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import com.nchernov.models.Genre;

public class Communicator {

	private HttpClient mClient = null;
	private String mHost = null;
	private String mPort = null;
	private JsonRequestProvider mJsonRequestProvider;
	private static final String JSON_CONTENT_KEY = "json";
	
	private static final String JSON_ANSWER_KEY = "status"; // ok
	private static final String JSON_ANSWER_KEY_OK = "ok"; // ok
	private static final String JSON_ANSWER_ERROR = "error";
	private static final String JSON_TOKEN_KEY = "uid";
	private static final int TIMEOUT = 30000;
	// 8b0fdedca0fdc7ace15b33d556c1cd5d
	private static final int LOGIN = 0;
	private static final int GENRES_LIST = 1;
	
	public Communicator(String mHost, String mPort) {
		mClient = new DefaultHttpClient();
		this.mHost = mHost;
		this.mPort = mPort;
		
	}
	
	/**
	 * Auth procedure to get token for performing furhter actions on other screen
	 * @param password
	 * @param userName
	 * @return uid unique String
	 * @throws UnsupportedEncodingException 
	 * @throws ConnectTimeoutException 
	 * @throws AuthException 
	 * @throws WrongRequestException 
	 */
	public String login(String password, String userName) throws UnsupportedEncodingException, ConnectTimeoutException, WrongRequestException, AuthException {
		String jsonRequest;
		IResponse loginResponse = null;
		try {
			jsonRequest = mJsonRequestProvider.createRequestForLogin(userName, password);
			loginResponse = sendRequest(jsonRequest, LOGIN);
			String error = loginResponse.getError();
			if (error != null) {
				// ERROR HERE!!!!
				return null;
			} else {
				String token = (String) loginResponse.getEntity(JsonResponse.ENTITY_TOKEN);
				return token;
			}

		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
			return null;
			// ERRROR!!!!!!!
		} catch (ConnectTimeoutException e) {
			throw new ConnectTimeoutException("Connection timeout exceeded");
		}
		
	}
	
	private IResponse sendRequest(String jsonRequest, int REQUEST_ID) throws JSONException, UnsupportedEncodingException, 
	ConnectTimeoutException, WrongRequestException, AuthException {
		IResponse customResponse = null;
		
		String uri = (mPort == null) ? mHost : mHost + ":" + mPort;
		HttpPost loginPost = new HttpPost(uri);
		List <NameValuePair> nvps = new ArrayList <NameValuePair>();
		nvps.add(new BasicNameValuePair(JSON_CONTENT_KEY, jsonRequest));
		HttpParams params = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(params, 3000);
		HttpConnectionParams.setSoTimeout(params, 5000);
		loginPost.setEntity(new UrlEncodedFormEntity(nvps));
		HttpResponse response = null;
		String jsonString = null;
		try {
			response = mClient.execute(loginPost);
			jsonString = convertStreamToString(response.getEntity().getContent());
		}  
		catch (ClientProtocolException e) {
			//e.printStackTrace();
		} catch (IOException e) {
			//e.printStackTrace();
		}
		
		switch (REQUEST_ID) {
		case LOGIN:
			customResponse = JsonResponseParser.parseJsonAuth(jsonString);
			break;
		case GENRES_LIST:
			customResponse = JsonResponseParser.parseJsonGenresList(jsonString);
			break;
			default:
				break;
		}
		
		return customResponse;
	}
	
	public List<Genre> requestForGenres(String token) throws UnsupportedEncodingException, ConnectTimeoutException, WrongRequestException, AuthException {
		String jsonRequest;
		IResponse genresResponse = null;
		try {
			jsonRequest = mJsonRequestProvider.createRequestForGenresList(token);
			genresResponse = sendRequest(jsonRequest, GENRES_LIST);
			String error = genresResponse.getError();
			if (error != null) {
				// ERROR HERE!!!!
				return null;
			} else {
				List<Genre> genres = (List<Genre>) genresResponse.getEntity(JsonResponse.ENTITY_GENRES_LIST);
				return genres;
			}

		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
			return null;
			// ERRROR!!!!!!!
		} catch (ConnectTimeoutException e) {
			throw new ConnectTimeoutException("Connection timeout exceeded");
		}
	}
	
	private String convertStreamToString(InputStream is) {
		/*
		
		 */
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
}
