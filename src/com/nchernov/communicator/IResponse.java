package com.nchernov.communicator;

public interface IResponse {

	public void addError(String error);
	public String getError();
	public void addEntity(String key, Object entity);
	public Object getEntity(String key);
}
