package com.nchernov.communicator;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonRequestProvider {

	protected static final int LOG_IN = 0;
	protected static final int GENRES_LIST = 1;
	
	private static final String COMMAND = "command";
	
	private static final String LOG_IN_COMMAND_AUTH = "auth";
	private static final String COMMAND_GENRES = "list_genres";
	private static final String GENRES_TOKEN = "uid";
	
	private static final String LOG_IN_LOGIN = "login";
	private static final String LOG_IN_PASSWORD = "password";
	
	public static String createRequestForLogin(String userName, String password) throws JSONException {
		JSONObject jsonRequest = new JSONObject();
		jsonRequest.put(COMMAND, LOG_IN_COMMAND_AUTH);
		jsonRequest.put(LOG_IN_LOGIN, userName);
		jsonRequest.put(LOG_IN_PASSWORD, password);
		
		return jsonRequest.toString();
	}
	
	public static String createRequestForGenresList(String token) throws JSONException {
		JSONObject jsonRequest = new JSONObject();
		jsonRequest.put(COMMAND, COMMAND_GENRES);
		jsonRequest.put(GENRES_TOKEN, token);
		
		return jsonRequest.toString();
	}
}
