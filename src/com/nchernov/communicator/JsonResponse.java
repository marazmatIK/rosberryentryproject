package com.nchernov.communicator;

import java.util.List;

import com.nchernov.models.Genre;

/**
 * Service-related implementation of json-serialized answers
 * @author zugzug
 *
 */
public class JsonResponse implements IResponse {

	protected static final String ENTITY_TOKEN = "token";
	protected static final String ENTITY_GENRES_LIST = "genres_list";
	private String mError = null;
	private String mToken;
	private List<Genre> mGenres = null;
	
	public JsonResponse(String error) {
		mError = error;
	}
	
	@Override
	public void addError(String error) {
		mError = error;

	}

	@Override
	public String getError() {
		return mError;
	}

	@Override
	public void addEntity(String key, Object entity) {
		if (key.equals(ENTITY_TOKEN)) {
			mToken = (String) entity;
		} else if (key.equals(ENTITY_GENRES_LIST)) {
			mGenres = (List<Genre>) entity;
		}
	}

	@Override
	public Object getEntity(String key) {
		if (key.equals(ENTITY_TOKEN)) {
			return mToken;
		} else if (key.equals(ENTITY_GENRES_LIST)) {
			return mGenres;
		} else return null;
	}

}
