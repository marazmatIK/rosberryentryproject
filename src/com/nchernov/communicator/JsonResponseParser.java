package com.nchernov.communicator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.nchernov.models.Genre;

public class JsonResponseParser {

	private static final String JSON_ANSWER_KEY = "status"; // ok
	private static final String JSON_ANSWER_KEY_OK = "ok"; // ok
	private static final String JSON_ANSWER_ERROR = "error";
	private static final String JSON_TOKEN_KEY = "uid";
	private static final String JSON_GENRES_LIST = "list";
	private static final String JSON_GENRE_ID = "id";
	private static final String JSON_GENRE_NAME = "name";
	
	public static IResponse parseJsonAuth(String jsonString) throws JSONException, WrongRequestException, AuthException {
		JSONObject json = buildJson(jsonString);
		IResponse response = null;
		String errorPossible = processForErrors(json);
		if (errorPossible != null) {
			response = new JsonResponse(errorPossible);
		} else {
			String token = json.getString(JSON_TOKEN_KEY);
			response = new JsonResponse(null);
			response.addEntity(JsonResponse.ENTITY_TOKEN, token);
		}
		
		return response;
	}
	
	public static IResponse parseJsonGenresList(String jsonString) throws JSONException, WrongRequestException, AuthException {
		JSONObject json = buildJson(jsonString);
		IResponse response = null;
		String errorPossible = processForErrors(json);
		List<Genre> genres = null;
		if (errorPossible != null) {
			response = new JsonResponse(errorPossible);
		} else {
			//String token = json.getString(JSON_TOKEN_KEY);
			//response = new JsonResponse(null);
			//response.addEntity(JsonResponse.ENTITY_TOKEN, token);
			JSONArray genresJson = json.getJSONArray(JSON_GENRES_LIST);
			JSONObject currentItem = null;
			genres = new ArrayList<Genre>();
			for (int i = 0; i < genresJson.length(); i++) {
				currentItem = (JSONObject) genresJson.get(i);
				Genre newGenre = new Genre(currentItem.getInt(JSON_GENRE_ID), currentItem.getString(JSON_GENRE_NAME));
				genres.add(newGenre);
			}
			response = new JsonResponse(null);
			response.addEntity(JsonResponse.ENTITY_GENRES_LIST, genres);
		}
		
		return response;
	}
	
	private static JSONObject buildJson(String jsonString) {
		try {
			JSONObject jsonToken = new JSONObject(jsonString);
			return jsonToken;
			
		} 
		catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private static boolean hasErrors(JSONObject json) {
		String key = null;
		Iterator iterator = json.keys();
		while (iterator.hasNext()) {
			key = (String) iterator.next();
			if (key.equals(JSON_ANSWER_ERROR)) {
				return true;
			}
		}
		
		return false;
	}
	
	private static boolean hasStatusAnswer(JSONObject json) {
		String key = null;
		Iterator iterator = json.keys();
		while (iterator.hasNext()) {
			key = (String) iterator.next();
			if (key.equals(JSON_ANSWER_KEY)) {
				return true;
			}
		}
		
		return false;
	}
	
	private static String processForErrors(JSONObject jsonResponse) throws JSONException, WrongRequestException, AuthException {
		String errorMessage = null;
		if (hasErrors(jsonResponse)) {
			String error = jsonResponse.getString(JSON_ANSWER_ERROR);
			errorMessage = "Error in request! " + jsonResponse.getString(JSON_ANSWER_ERROR);
			if (error.contains("no request") || error.contains("invalid request")) {
				throw new WrongRequestException();
			} else if (error.contains("auth error")) {
				throw new AuthException();
			}
		} else {
			if (!hasStatusAnswer(jsonResponse)) return errorMessage;
			String status = jsonResponse.getString(JSON_ANSWER_KEY);
			if (!status.equals(JSON_ANSWER_KEY_OK))  {
				errorMessage = "Error in request! Status: " + jsonResponse.getString(JSON_ANSWER_ERROR);
			}
		}
		
		return errorMessage;
	}
}
