package com.nchernov.models;

public class Genre {

	private String mName = null;
	private int mID = -1;
	
	public Genre(String name) {
		mName = name;
	}
	
	public Genre(int mID, String mName) {
		this(mName);
		this.mID = mID;
		
	}

	public String getName() {
		return mName;
	}
	
	public String toString() {
		return mName;
	}
	
	public int getID() {
		return mID;
	}
}
