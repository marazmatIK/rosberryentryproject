package com.nchernov.models;

import java.util.ArrayList;
import java.util.List;

public class GenreManager {

	private GenresDAO mGenresDAO = null;
	private List<Genre> mGenres;
	
	public GenreManager(List<Genre> genres) {
		mGenres = genres;
	}
	
	public List<Genre> findByStartSubstring(String substring) {
		List<Genre> subSet = new ArrayList<Genre>();
		for (int i = 0; i < mGenres.size(); i++) {
			Genre candidate = null;
			candidate = mGenres.get(i);
			if (candidate.getName().toLowerCase().startsWith(substring.toLowerCase())) {
				subSet.add(candidate);
			}
		}
		
		return subSet;
	}
}
