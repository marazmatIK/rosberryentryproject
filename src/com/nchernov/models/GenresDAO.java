package com.nchernov.models;

import java.util.Collection;

public interface GenresDAO {

	public boolean saveGenre(Genre genre);
	public boolean saveGenres(Collection<Genre> genres);
	public Collection<Genre> loadGenres();
}
