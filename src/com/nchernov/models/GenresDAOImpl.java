package com.nchernov.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class GenresDAOImpl extends SQLiteOpenHelper implements GenresDAO {
	protected static final int DATABASE_VERSION = 1;
	protected static final String DATABASE_NAME = "genresDB.db";
	protected static final String TABLE_GENRES = "genres";
	private static final String GENRE_ID = "genre_id";
	private static final String GENRE_NAME = "genre_name";
	private static final String CREATE_GENRES_TABLE = "CREATE TABLE " +
            TABLE_GENRES + "("
            + GENRE_ID + " INTEGER PRIMARY KEY," + GENRE_NAME 
            + " TEXT" + ")";
	
	private static final String DROP = "DROP TABLE IF EXISTS " + TABLE_GENRES;
			
	protected GenresDAOImpl(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
	}
	
	public GenresDAOImpl(Context context) {
		this(context, DATABASE_NAME, null, DATABASE_VERSION);
		
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
	      db.execSQL(CREATE_GENRES_TABLE);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(DROP);
	      onCreate(db);
	}
	
	@Override
	public boolean saveGenre(Genre genre) {
		ContentValues values = new ContentValues();
        values.put(GENRE_ID, genre.getID());
        values.put(GENRE_NAME, genre.getName());
 
        SQLiteDatabase db = this.getWritableDatabase();
        
        db.insert(TABLE_GENRES, null, values);
        db.close();
        
        return true;
	}

	@Override
	public boolean saveGenres(Collection<Genre> genres) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL(DROP);
		db.execSQL(CREATE_GENRES_TABLE);
		
		for (Genre genre: genres) {
			ContentValues values = new ContentValues();
	        values.put(GENRE_ID, genre.getID());
	        values.put(GENRE_NAME, genre.getName());
			db.insert(TABLE_GENRES, null, values);
		}
		db.close();
		return true;
	}

	@Override
	public Collection<Genre> loadGenres() {
		String query = "Select * FROM " + TABLE_GENRES + " ORDER BY 1 ASC";
		
		SQLiteDatabase db = this.getWritableDatabase();
		
		Cursor cursor = db.rawQuery(query, null);
		List<Genre> genres = new ArrayList<Genre>();
		
		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
			Genre genre = new Genre(cursor.getInt(0), cursor.getString(1));
			genres.add(genre);
			cursor.moveToNext();
		}
	        db.close();
		return genres;
	}

}
