package com.nchernov.views;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

public class CustomLocationListener implements LocationListener {

	private final static String TAG = CustomLocationListener.class.getCanonicalName();
	Location mLocation;
	private double lng;
	private double lat;
	
	@Override
	public void onLocationChanged(Location ln) 
	{
	    lng=ln.getLongitude();
	    lat=ln.getLatitude();  
	    mLocation = ln;
	    
	    Log.d(TAG, String.format("Changed coords: [%f;%f]" , lng, lat));
	}
	@Override
	public void onProviderDisabled(String arg0) {  
	}
	@Override
	public void onProviderEnabled(String arg0) {   
	}
	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
	 }
	
	public double getLongitude() {
		return lng;
	}
	
	public double getLAttitude() {
		return lat;
	}
	
	public Location getLocation() {
		return mLocation;
	}

}
