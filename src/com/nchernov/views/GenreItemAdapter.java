package com.nchernov.views;

import java.util.List;

import com.nchernov.application.R;
import com.nchernov.models.Genre;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class GenreItemAdapter extends ArrayAdapter<Genre> {

	private List<Genre> mGenres = null;
	private LayoutInflater mInflater = null;
	public GenreItemAdapter(Context context,
			int layoutResourceId, List<Genre> genres) {
		super(context, layoutResourceId, genres);
		mGenres = genres;
		mInflater  = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder = null;
		
		if (convertView==null)
		{
		    
			convertView = mInflater.inflate(R.layout.genre_row, null);
		    holder = new ViewHolder();
		    
		    holder.viewGenreName = (TextView)convertView.findViewById(R.id.genre_name);
		    holder.viewGenreId = (TextView)convertView.findViewById(R.id.genre_ID);
		  
		    convertView.setTag(holder);
		}
		else
		{
		    holder = (ViewHolder) convertView.getTag();
		}
		Genre genre = mGenres.get(position);
		holder.viewGenreName.setText(genre.toString());
		holder.viewGenreId.setText(genre.getID() + "");
		
		return convertView;
		
	}

	private class ViewHolder
	{
		TextView viewGenreName = null;
		TextView viewGenreId = null;
	}

}
